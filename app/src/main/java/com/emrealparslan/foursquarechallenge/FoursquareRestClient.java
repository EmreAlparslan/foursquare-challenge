package com.emrealparslan.foursquarechallenge;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONException;

/**
 * Created by Emre on 10.12.2016.
 */

public class FoursquareRestClient {

    private static AsyncHttpClient client = new SyncHttpClient();

    public static void get(Context context, String url, AsyncHttpResponseHandler responseHandler) throws JSONException {
        client.get(context,url, responseHandler);
    }
}
