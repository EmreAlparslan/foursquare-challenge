package com.emrealparslan.foursquarechallenge;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.emrealparslan.foursquarechallenge.Model.User;
import com.google.gson.Gson;

import mehdi.sakout.fancybuttons.FancyButton;

public class LoginActivity extends AppCompatActivity {

    private EditText nameEditText, emailEditText, passwordEditText;
    private FancyButton signupAndLoginButton;
    private User currentUser;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Creating a shared preference
        sharedPreferences = getPreferences(MODE_PRIVATE);

        //Checking if there is a UserObject
        if (sharedPreferences.contains("UserObject")) {
            Gson gson = new Gson();
            String json = sharedPreferences.getString("UserObject", "");
            currentUser = gson.fromJson(json, User.class);
        }

        if (currentUser != null) {
            //Starting new Intent
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("currentUser", currentUser);
            intent.putExtras(bundle);
            startActivity(intent);
        }

        initViews();
    }

    private void initViews() {
        nameEditText = (EditText) findViewById(R.id.name_edit_text);
        emailEditText = (EditText) findViewById(R.id.email_edit_text);
        passwordEditText = (EditText) findViewById(R.id.password_edit_text);
        signupAndLoginButton = (FancyButton) findViewById(R.id.signup_login_button);

        signupAndLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameStr = nameEditText.getText().toString().trim();
                String emailStr = passwordEditText.getText().toString().trim();
                String passwordStr = nameEditText.getText().toString();

                //Creating User object with inputs
                User currentUser = new User();
                currentUser.setName(nameStr);
                currentUser.setEmail(emailStr);
                currentUser.setPassword(passwordStr);

                //Saving User object to SharedPreferences in private mode.
                SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
                Gson gson = new Gson();
                String json = gson.toJson(currentUser);
                prefsEditor.putString("UserObject", json);
                prefsEditor.commit();

                //Starting new Intent
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("currentUser", currentUser);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
    }


}
