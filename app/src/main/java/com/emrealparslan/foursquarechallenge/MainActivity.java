package com.emrealparslan.foursquarechallenge;

import android.Manifest;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.emrealparslan.foursquarechallenge.Model.User;
import com.emrealparslan.foursquarechallenge.Model.Venue;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.util.ByteArrayBuffer;

public class MainActivity extends AppCompatActivity {

    // the foursquare client_id and the client_secret
    final String CLIENT_ID = "33ZPFB2GLYHOJPUZOX0UQ55EVSBFZNTVYN0FDVNPF0A5MFD2";
    final String CLIENT_SECRET = "1DPFTITAR2NLQIJRQDOUTHFCW4C4ULJUR0CVG5E5VUXWZXGU";
    private User currentUser;
    private boolean isSearchOpened;
    private DrawerLayout drawerLayout;
    private CoordinatorLayout leftDrawerLayout;
    private RecyclerView rView;
    private ListView leftDrawerListView;
    private Toolbar toolbar;
    private int toolbarPos;
    private BroadcastReceiver connectionChangeReceiver;
    private LocationManager locationManager;
    private List<Venue> venues;
    private int connectionType;
    // we will need to take the latitude and the logntitude from a certain point
    // this is the center of New York
    private double latitude = 0.0;
    private double longtitude = 0.0;

    public static String makeCall(String url) {

        // string buffers the url
        StringBuffer buffer_string = new StringBuffer(url);
        String replyString = "";

        // instanciate an HttpClient
        HttpClient httpclient = HttpClientBuilder.create().build();
        // instanciate an HttpGet
        HttpGet httpget = new HttpGet(buffer_string.toString());

        try {
            // get the responce of the httpclient execution of the url
            HttpResponse response = httpclient.execute(httpget);
            InputStream is = response.getEntity().getContent();

            // buffer input stream the result
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }
            // the result as a string is ready for parsing
            replyString = new String(baf.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        // trim the whitespaces
        return replyString.trim();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        changeActionBar(0);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.openDrawer(leftDrawerLayout);
            }
        });

        isSearchOpened = false;

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            currentUser = (User) bundle.getSerializable("currentUser");
            Log.i("user", currentUser.getName());
        }

        initNavigationDrawer();

        rView = (RecyclerView) findViewById(R.id.recycler_view);

        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        Location location = getLastKnownLocation();
        if(location != null){
            latitude = location.getLatitude();
            longtitude = location.getLongitude();
            Log.i("lng - lat", String.valueOf(latitude) + "," + String.valueOf(longtitude));
            venues = new ArrayList<>();
            new Foursquare().execute();
        }

    }

    private Location getLastKnownLocation() {
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {
                bestLocation = l;
            }
        }
        if (bestLocation == null) {
            return null;
        }
        return bestLocation;
    }

    private void initNavigationDrawer() {
        //Creating a ArrayList for drawerList
        List<String> listViewItemArray = new ArrayList<>();
        listViewItemArray.add(getString(R.string.near_by));
        listViewItemArray.add(getString(R.string.last_check_ins));
        //
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        leftDrawerLayout = (CoordinatorLayout) findViewById(R.id.left_drawer);
        leftDrawerListView = (ListView) findViewById(R.id.left_drawer_list_view);
        leftDrawerListView.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, listViewItemArray));
        // Set the list's click listener
        leftDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (toolbarPos != i) {
                    changeActionBar(i);
                }
                drawerLayout.closeDrawer(leftDrawerLayout);
                switch (i) {
                    case 0:

                        break;
                    case 1:

                        break;
                }
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case R.id.action_search: // should I need to add intent ?
                //handleMenuSearch();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isSearchOpened) {
            //handleMenuSearch();
            return;
        }
        super.onBackPressed();
    }

    private void changeActionBar(int pos) {
        switch (pos) {
            case 0:
                toolbar.setTitle(getString(R.string.near_by));
                toolbar.setLogo(R.drawable.ic_nearme);
                toolbar.setNavigationIcon(R.drawable.ic_nav);
                setSupportActionBar(toolbar);
                toolbarPos = 0;
                break;
            case 1:
                toolbar.setTitle(getString(R.string.last_check_ins));
                toolbar.setLogo(R.drawable.ic_check_in);
                toolbar.setNavigationIcon(R.drawable.ic_nav);
                setSupportActionBar(toolbar);
                toolbarPos = 1;
                break;
        }
    }

    private void checkInternetConnection() {

        if (connectionChangeReceiver == null) {

            connectionChangeReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {

                    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    if (activeNetwork != null) { // connected to the internet
                        if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                            // connected to wifi
                            connectionType = 0;
                            Log.d("Network", "Wifi");
                        } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                            // connected to the mobile provider's data plan
                            connectionType = 1;
                            Log.d("Network", "Mobile");
                        }
                    } else {
                        // not connected to the internet
                    }

                }
            };

            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver((BroadcastReceiver) connectionChangeReceiver, intentFilter);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkInternetConnection();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (connectionChangeReceiver != null) {
            unregisterReceiver(connectionChangeReceiver);
        }
    }

    private void parseJSON(String response) {
        if (response == null) {

        } else {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.has("response")) {
                    if (jsonObject.getJSONObject("response").has("venues")) {
                        venues.clear();
                        venues = new ArrayList<>();
                        JSONArray jsonArray = jsonObject.getJSONObject("response").getJSONArray("venues");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Gson gson = new Gson();
                            Venue venue = gson.fromJson(String.valueOf(jsonArray.get(i)), Venue.class);
                            venues.add(venue);
                            Log.i("venue", venue.toString());
                        }
                    }
                } else {
                    //
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private class Foursquare extends AsyncTask {

        String response;

        @Override
        protected Object doInBackground(Object[] objects) {
            // make Call to the url
            response = makeCall("https://api.foursquare.com/v2/venues/search?client_id=33ZPFB2GLYHOJPUZOX0UQ55EVSBFZNTVYN0FDVNPF0A5MFD2&client_secret=1DPFTITAR2NLQIJRQDOUTHFCW4C4ULJUR0CVG5E5VUXWZXGU&v=20130815&ll=" + String.valueOf(latitude) + "," + String.valueOf(longtitude));
            return "";
        }

        @Override
        protected void onPostExecute(Object o) {
            if (response == null) {

            } else {
                Log.i("response", response);
                parseJSON(response);
                //Reloading RecyclerView if venues is not null or empty
                if (venues != null || !venues.isEmpty()) {
                    //
                }
            }
        }
    }

}
