package com.emrealparslan.foursquarechallenge.Model;

/**
 * Created by Emre on 10.12.2016.
 */

public class Location {
    String address;
    double lat, lng;
    int distance;
    String city;
    String country;
}
