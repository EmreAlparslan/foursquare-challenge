package com.emrealparslan.foursquarechallenge.Model;

import java.util.List;

/**
 * Created by Emre on 10.12.2016.
 */

public class Venue {
    String id;
    String name;
    List<Category> categories;
    Location location;
    HereNow hereNow;
}
